package main.java.formulaire;

//Implémentation concrète pour le rendu des formulaires avec des widgets

public class RenduFormulaireWidget implements RenduFormulaire {    
 public void afficherChamps() {        
     System.out.println("Rendu des champs de formulaire avec des widgets");    
 }
 public void afficherBoutonSoumettre() {        
     System.out.println("Rendu du bouton de soumission avec des widgets");    
 }
}
