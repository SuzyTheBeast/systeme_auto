package main.java.commande;

public enum EtatCommande {
    EN_COURS,
    VALIDE,
    LIVREE
}
