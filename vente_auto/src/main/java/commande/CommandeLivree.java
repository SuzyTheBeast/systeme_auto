package main.java.commande;

public class CommandeLivree extends Commande{
	// Attributs spécifiques...

    @Override
    public double calculerTaxes() {
        // Implémentation spécifique pour les commandes livrées
    }

    @Override
    public String genererDocuments() {
        // Implémentation spécifique pour les commandes livrées
    }
}
