package main.java.commande;

//Classe abstraite pour la factory de commandes
public abstract class CommandeFactory {
	//Methode factory abstraite 
	public abstract Commande creerCommande();
}
