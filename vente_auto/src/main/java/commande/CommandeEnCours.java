package main.java.commande;

public class CommandeEnCours extends Commande {
	// Attributs spécifiques...

    @Override
    public double calculerTaxes() {
        // Implémentation spécifique pour les commandes en cours
    }

    @Override
    public String genererDocuments() {
        // Implémentation spécifique pour les commandes en cours
    }
}
