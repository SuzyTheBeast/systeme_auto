package main.java.commande;

public class CommandeValidee extends Commande {
	 // Attributs spécifiques...

    @Override
    public double calculerTaxes() {
        // Implémentation spécifique pour les commandes validées
    }

    @Override
    public String genererDocuments() {
        // Implémentation spécifique pour les commandes validées
    }
}
